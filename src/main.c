#include <stdint.h>
#include <time.h>
#include <wxc.h>

#include <wx/defs.h>

#include "sheep.h"

wmPanel *wmp;

void wmInitApp(void* fun, void* data, void* evt) {
    TClass(wxFrame) frame = wxFrame_Create(NULL, wxID_ANY, wxString_Create(L"sheep"),
            -1, -1, -1, -1, 0x0800 | 0x0040 | 0x0400 | 0x0200 | 0x1000 | 0x20000000 | 0x00400000);

    wmp = wmPanel_Create(frame);
    wmPanel_Start(wmp);

    wxWindow_Show(frame);
}

int main(int argc, char** argv) {
	ELJApp_InitializeC(wxClosure_Create((TClosureFun) wmInitApp, NULL), argc, argv);
	return 0;
}

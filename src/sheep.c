
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sheep.h"
#include "sheep_img.h"

static const int DEFAULT_FRAME_WIDTH = 120;

static const int DEFAULT_FRAME_HEIGHT = 120;

static const int SHEEP_MOVE_X_DELTA = 5;

static const int SHEEP_MOVE_Y_DELTA = 3;

TClass(wxBitmap) wmImage_GetScaledBitmap(TClass(wxImage) image, int scale);

wmSheep* wmSheep_Create(wmPanel* wmp)
{
	wmSheep* sheep = wxcMalloc(sizeof(wmSheep));
	if (sheep) {
		memset(sheep, 0, sizeof(wmSheep));

		TClass(wxSize) psize = wxWindow_GetSize(wmp->panel);
		TClass(wxSize) gsize = wmp->ground_size;
		TClass(wxSize) fsize = wmp->fence_size;

		int width = sheep00_width * wmp->scale;
		int height = sheep00_height * wmp->scale;
		int x = wxSize_GetWidth(psize) + width;
		int y = wxSize_GetHeight(psize) - random() % wxSize_GetHeight(gsize) - height;

		sheep->point = wxPoint_Create(x, y);
		sheep->size = wxSize_Create(width, height);
		sheep->jump_x = -1 * (y - wxSize_GetHeight(psize)) * wxSize_GetWidth(fsize) / wxSize_GetHeight(fsize) + (wxSize_GetWidth(psize) - wxSize_GetWidth(fsize)) / 2;
		sheep->scale = wmp->scale;

		wxSize_Delete(psize);
	}

	return sheep;
}

void wmSheep_Delete(wmSheep* sheep)
{
	wxcFree(sheep);
}

wmSheep* wmSheep_Run(wmSheep* sheep)
{
	wxPoint_SetX(sheep->point, wxPoint_GetX(sheep->point) - SHEEP_MOVE_X_DELTA * sheep->scale);

	if (wxPoint_GetX(sheep->point) < sheep->jump_x && sheep->jump_state < 3) {
		wxPoint_SetY(sheep->point, wxPoint_GetY(sheep->point) - SHEEP_MOVE_Y_DELTA * sheep->scale);
		sheep->strech_leg = 1;
		sheep->jump_state++;
	} else if (wxPoint_GetX(sheep->point) < sheep->jump_x && sheep->jump_state < 6) {
		wxPoint_SetY(sheep->point, wxPoint_GetY(sheep->point) + SHEEP_MOVE_Y_DELTA * sheep->scale);
		sheep->strech_leg = 1;
		sheep->jump_state++;
	} else {
		sheep->strech_leg = 1 - sheep->strech_leg;
	}

	return sheep;
}

int wmSheep_IsGoAway(wmSheep* sheep)
{
	if (sheep == NULL) return -1;

	if (wxPoint_GetX(sheep->point) < -1 * wxSize_GetWidth(sheep->size)) {
		return 1;
	} else {
		return 0;
	}
}

wmSheepFlock* wmSheepFlock_Create()
{
	wmSheepFlock* flock = wxcMalloc(sizeof(wmSheepFlock));
	if (flock) {
		flock->sheep = NULL;
		flock->next = NULL;
	}
	return flock;
}

void wmSheepFlock_Delete(wmSheepFlock* flock)
{
	if (flock == NULL)  return;

	if (flock->next == NULL) {
		wmSheep_Delete(flock->sheep);
	} else {
		wmSheepFlock_Delete(flock->next);
	}
	wxcFree(flock);
}

wmSheepFlock* wmSheepFlock_AddSheep(wmSheepFlock* flock, wmSheep* sheep)
{
	wmSheepFlock* cursor;
	for (cursor = flock; cursor->next != NULL; cursor = (wmSheepFlock*) cursor->next) {
	}

	if (cursor->sheep == NULL) {
		cursor->sheep = sheep;
	} else {
		cursor->next = wmSheepFlock_Create();
		((wmSheepFlock*) (cursor->next))->sheep = sheep;
	}

	return flock;
}

int wmSheepFlock_GetSize(wmSheepFlock *flock)
{
	int size;

	if (flock == NULL) {
		return -1;
	} else if (flock->sheep == NULL) {
		return 0;
	} else {
		size = 1;
	}

	for (wmSheepFlock* cursor = flock; cursor->next != NULL; cursor = (wmSheepFlock*) cursor->next) {
		wmSheep* sheep = cursor->sheep;
		if (sheep == NULL) {
			break;
		}

		size++;
	}

	return size;
}

wmPanel* wmPanel_Create(TClass(wxFrame) frame)
{
	wmPanel* wmp = (wmPanel*) wxcMalloc(sizeof(wmPanel));
	if (wmp == NULL) {
		return NULL;
	}

	wmp->panel = wxPanel_Create(frame, expwxID_ANY(), -1, -1, -1, -1, 0);
	if (wmp->panel == NULL) {
		wxcFree(wmp);
		return NULL;
	}

	wmp->sheep_flock = wmSheepFlock_Create();
	if (wmp->sheep_flock == NULL) {
		wxcFree(wmp);
		return NULL;
	}

	wxCommandEvent_Create(0, 0);
	wxImage_AddHandler(wxImageHandler_CreateFromStock(5));

	wmp->count = 0;
	wmp->scale = 0;
	wmp->running = 0;
	wmp->timer = NULL;
	wmp->sheep_flock = NULL;
	wmp->ground_point = wxPoint_Create(0, 0);
	wmp->ground_size = wxSize_Create(0, 0);
	wmp->fence_point = wxPoint_Create(0, 0);
	wmp->fence_size = wxSize_Create(0, 0);
	wmp->sheep_size = wxSize_Create(sheep00_width, sheep00_height);

	wmp->sky_colour = wxColour_CreateRGB(150, 150, 255, 0xFF);
	wmp->ground_colour = wxColour_CreateRGB(150, 255, 150, 0xFF);

	wmp->sheep00_image = wxImage_CreateSized(sheep00_width, sheep00_height);
	wxImage_LoadStream(wmp->sheep00_image,
			wxMemoryInputStream_Create(sheep00_png, sheep00_png_len),
			expwxBITMAP_TYPE_ANY(), -1);
	wxImage_InitAlpha(wmp->sheep00_image);

	wmp->sheep01_image = wxImage_CreateSized(sheep01_width, sheep01_height);
	wxImage_LoadStream(wmp->sheep01_image,
			wxMemoryInputStream_Create(sheep01_png, sheep01_png_len),
			expwxBITMAP_TYPE_ANY(), -1);
	wxImage_InitAlpha(wmp->sheep01_image);

	wmp->fence_image = wxImage_CreateSized(fence_width, fence_height);
	wxImage_LoadStream(wmp->fence_image,
			wxMemoryInputStream_Create(fence_png, fence_png_len),
			expwxBITMAP_TYPE_ANY(), -1);
	wxImage_InitAlpha(wmp->fence_image);

    wxEvtHandler_Connect(wmp->panel, expwxID_ANY(), expwxID_ANY(),
    		expEVT_SIZE(), wxClosure_Create((TClosureFun) wmPanel_OnSize, wmp));
    wxEvtHandler_Connect(wmp->panel, expwxID_ANY(), expwxID_ANY(),
    		expEVT_TIMER(), wxClosure_Create((TClosureFun) wmPanel_OnUpdate, wmp));
    wxEvtHandler_Connect(wmp->panel, expwxID_ANY(), expwxID_ANY(),
    		expEVT_PAINT(), wxClosure_Create((TClosureFun) mwPanel_OnPaint, wmp));

    return wmp;
}

void wmPanel_Start(wmPanel* wmp)
{
	wmp->timer = wxTimer_Create(wmp->panel, expwxID_ANY());
	wxTimer_Start(wmp->timer, 100, 0);
}

void wmPanel_Stop(wmPanel* wmp)
{
	wxTimer_Stop(wmp->timer);
	wmp->timer = NULL;
}

void wmPanel_OnSize(TClosureFun fun, void* data, TClass(wxSizeEvent) evt)
{
	wmPanel* wmp = (wmPanel*) data;
    if (evt == NULL) {
        return;
    }

    TClass(wxSize) size = wxSizeEvent_GetSize(evt);
    int width = wxSize_GetWidth(size);
    int height = wxSize_GetHeight(size);

	int scale;
	int ratioWidth = width / DEFAULT_FRAME_WIDTH;
	int ratioHeight = height / DEFAULT_FRAME_HEIGHT;
	if (ratioWidth > ratioHeight) {
		scale = ratioHeight;
	} else {
		scale = ratioWidth;
	}
	if (scale <= 0) {
		scale = 1;
	}

	if (wmp->scale != scale) {
		wmp->scale = scale;

		wmp->sheep00_bitmap = wmImage_GetScaledBitmap(wmp->sheep00_image, scale);
		wmp->sheep01_bitmap = wmImage_GetScaledBitmap(wmp->sheep01_image, scale);
		wmp->fence_bitmap = wmImage_GetScaledBitmap(wmp->fence_image, scale);
	}

	wxSize_SetWidth(wmp->fence_size, fence_width * scale);
	wxSize_SetHeight(wmp->fence_size, fence_height * scale);
	wxPoint_SetX(wmp->fence_point, (width - wxSize_GetWidth(wmp->fence_size)) / 2);
	wxPoint_SetY(wmp->fence_point, height - wxSize_GetHeight(wmp->fence_size));

	wxSize_SetWidth(wmp->ground_size, width);
	wxSize_SetHeight(wmp->ground_size, (int) (wxSize_GetHeight(wmp->fence_size) * 0.9f));
	wxPoint_SetX(wmp->ground_point, 0);
	wxPoint_SetY(wmp->ground_point, height - wxSize_GetHeight(wmp->ground_size));

	wmSheepFlock_Delete(wmp->sheep_flock);
	wmp->sheep_flock = wmSheepFlock_Create();

}

TClass(wxBitmap) wmImage_GetScaledBitmap(TClass(wxImage) image, int scale)
{
	TClass(wxImage) scaled_image = wxImage_CreateDefault();
	wxImage_Scale(image, wxImage_GetWidth(image) * scale, wxImage_GetHeight(image) * scale, scaled_image);

	TClass(wxBitmap) bitmap;
	if (scaled_image) {
		bitmap = wxBitmap_CreateFromImage(scaled_image, -1);
		wxImage_Delete(scaled_image);
	} else {
		bitmap = NULL;
	}
	return bitmap;
}

void wmPanel_OnUpdate(TClosureFun fun, void* data, TClass(wxTimerEvent) evt)
{
	wmPanel* wmp = (wmPanel*) data;
	if (evt == NULL) {
		return;
	}

	if (wmp->sheep_flock->sheep != NULL && wmp->sheep_flock->next == NULL) {
		wmp->sheep_flock->sheep = wmSheep_Run(wmp->sheep_flock->sheep);
		if (wmp->sheep_flock->sheep->jump_state == 3) {
			wmp->count++;
		}

		if (wmSheep_IsGoAway(wmp->sheep_flock->sheep)) {
			wmSheep_Delete(wmp->sheep_flock->sheep);
			wmp->sheep_flock->sheep = NULL;
		}
	} else {
		for (wmSheepFlock* cursor = wmp->sheep_flock; cursor->next != NULL; cursor = (wmSheepFlock*) cursor->next) {
			if (cursor->sheep != NULL) {
				cursor->sheep = wmSheep_Run(cursor->sheep);

				if (cursor->sheep->jump_state == 3) {
					wmp->count++;
				}
			}
		}
	}

	if (wmp->sheep_flock->sheep == NULL) {
		wmp->sheep_flock = wmSheepFlock_AddSheep(wmp->sheep_flock, wmSheep_Create(wmp));
	}

	wxWindow_Refresh(wmp->panel, 1);
}

void wmPanel_DrawSheep(wmPanel* wmp, TClass(wxPaintDC) dc, wmSheep* sheep)
{

	if (sheep->strech_leg) {
		wxDC_DrawBitmap(dc, wmp->sheep01_bitmap, wxPoint_GetX(sheep->point), wxPoint_GetY(sheep->point), 0);
	} else {
		wxDC_DrawBitmap(dc, wmp->sheep00_bitmap, wxPoint_GetX(sheep->point), wxPoint_GetY(sheep->point), 0);
	}

}

void mwPanel_OnPaint(TClosureFun fun, void* data, TClass(wxPaintEvent) evt)
{
	wmPanel* wmp = (wmPanel*) data;
	if (evt == NULL || wmp == NULL || wmp->scale <= 0) {
		return;
	}

	TClass(wxPaintDC) dc = wxPaintDC_Create(wmp->panel);
	TClass(wxBrush) brush = wxBrush_CreateDefault();
	wxDC_SetPen(dc, wxPen_CreateFromStock(5));

	TClass(wxSize) size = wxWindow_GetSize(wmp->panel);

	if (wmp->sky_colour) {
		wxBrush_SetColour(brush, wmp->sky_colour);
		wxDC_SetBrush(dc, brush);
		wxDC_DrawRectangle(dc, 0, 0, wxSize_GetWidth(size), wxSize_GetHeight(size));
	}

	if (wmp->ground_colour) {
		wxBrush_SetColour(brush, wmp->ground_colour);
		wxDC_SetBrush(dc, brush);
		wxDC_DrawRectangle(dc,
				wxPoint_GetX(wmp->ground_point), wxPoint_GetY(wmp->ground_point),
				wxSize_GetWidth(wmp->ground_size), wxSize_GetHeight(wmp->ground_size));
	}

	if (wmp->fence_bitmap) {
		wxDC_DrawBitmap(dc, wmp->fence_bitmap,
				wxPoint_GetX(wmp->fence_point), wxPoint_GetY(wmp->fence_point), 0);
	}

	if (wmp->sheep_flock == NULL || wmp->sheep_flock->sheep == NULL) {
	} else if (wmp->sheep_flock->next == NULL) {
		wmPanel_DrawSheep(wmp, dc, wmp->sheep_flock->sheep);
	} else {
		for (wmSheepFlock* cursor = wmp->sheep_flock; cursor->next != NULL; cursor = (wmSheepFlock*) cursor->next) {
			wmSheep* sheep = cursor->sheep;
			if (sheep == NULL) {
				break;
			}
			wmPanel_DrawSheep(wmp, dc, cursor->sheep);
		}
	}

	char count[100];
	sprintf(count, "%d sheep", wmp->count);
	wchar_t* wcount = (wchar_t*) wxcMalloc(sizeof(wchar_t) * strlen(count + 1));
	mbstowcs(wcount, count, strlen(count) + 1);
	TClass(wxString) counter = wxString_Create(wcount);
	wxDC_DrawText(dc, counter, 5, 5);
	wxString_Delete(counter);

	wxSize_Delete(size);
	wxBrush_Delete(brush);
	wxPaintDC_Delete(dc);
}

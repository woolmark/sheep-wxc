
#include <stdint.h>
#include <time.h>
#include <wxc.h>

typedef struct {
	TClass(wxPoint) point;
	TClass(wxSize) size;
	int scale;
	int strech_leg;
	int jump_x;
	int jump_state;
} wmSheep;

typedef struct {
	wmSheep* sheep;
	void* next;
} wmSheepFlock;

typedef struct {

	TClass(wxPanel) panel;

	int running;

	int scale;

	int count;

	wmSheepFlock* sheep_flock;

	TClass(wxTimer) timer;

	TClass(wxPoint) ground_point;

	TClass(wxSize) ground_size;

	TClass(wxPoint) fence_point;

	TClass(wxSize) fence_size;

	TClass(wxSize) sheep_size;

	TClass(wxColour) sky_colour;

	TClass(wxColour) ground_colour;

	TClass(wxImage) sheep00_image;

	TClass(wxImage) sheep01_image;

	TClass(wxImage) fence_image;

	TClass(wxBitmap) sheep00_bitmap;

	TClass(wxBitmap) sheep01_bitmap;

	TClass(wxBitmap) fence_bitmap;

} wmPanel;

wmPanel* wmPanel_Create(TClass(wxFrame) frame);

void wmPanel_Start(wmPanel*);

void wmPanel_Stop(wmPanel*);

void wmPanel_OnSize(TClosureFun fun, void* data, TClass(wxSizeEvent) evt);

void wmPanel_OnUpdate(TClosureFun fun, void* data, TClass(wxCommandEvent) evt);

void mwPanel_OnPaint(TClosureFun fun, void* data, TClass(wxPaintEvent) evt);


CC = `wx-config --cc`
CC_FLAGS = -Wall -Wextra `wx-config --cflags` -IwxHaskell/wxc/src/include -Iinc
CXX	= `wx-config --cxx`
CXX_FLAGS = -Wall -Wextra -pedantic -std=c++11 -pthread `wx-config --cxxflags` -IwxHaskell/wxc/src/include
CXX_RELEASE_FLAGS = -O3 -march=native
CXX_DEBUG_FLAGS= -g
LD = `wx-config --ld`
LD_FLAGS = `wx-config --libs all`

BIN_FILE = sheep
OBJ_FILES = src/main.o src/sheep.o

WXC_OBJ_FILES = \
    wxHaskell/wxc/src/cpp/apppath.o \
    wxHaskell/wxc/src/cpp/dragimage.o \
    wxHaskell/wxc/src/cpp/eljaccelerator.o \
    wxHaskell/wxc/src/cpp/eljartprov.o \
    wxHaskell/wxc/src/cpp/eljaui.o \
    wxHaskell/wxc/src/cpp/eljbitmap.o \
    wxHaskell/wxc/src/cpp/eljbrush.o \
    wxHaskell/wxc/src/cpp/eljbusyinfo.o \
    wxHaskell/wxc/src/cpp/eljbutton.o \
    wxHaskell/wxc/src/cpp/eljcalendarctrl.o \
    wxHaskell/wxc/src/cpp/eljcaret.o \
    wxHaskell/wxc/src/cpp/eljcheckbox.o \
    wxHaskell/wxc/src/cpp/eljchecklistbox.o \
    wxHaskell/wxc/src/cpp/eljchoice.o \
    wxHaskell/wxc/src/cpp/eljclipboard.o \
    wxHaskell/wxc/src/cpp/eljcoldata.o \
    wxHaskell/wxc/src/cpp/eljcolour.o \
    wxHaskell/wxc/src/cpp/eljcolourdlg.o \
    wxHaskell/wxc/src/cpp/eljcombobox.o \
    wxHaskell/wxc/src/cpp/eljcommand.o \
    wxHaskell/wxc/src/cpp/eljconfigbase.o \
    wxHaskell/wxc/src/cpp/eljcontrol.o \
    wxHaskell/wxc/src/cpp/eljctxhelp.o \
    wxHaskell/wxc/src/cpp/eljcursor.o \
    wxHaskell/wxc/src/cpp/eljdataformat.o \
    wxHaskell/wxc/src/cpp/eljdatetime.o \
    wxHaskell/wxc/src/cpp/eljdc.o \
    wxHaskell/wxc/src/cpp/eljdcsvg.o \
    wxHaskell/wxc/src/cpp/eljdialog.o \
    wxHaskell/wxc/src/cpp/eljdirdlg.o \
    wxHaskell/wxc/src/cpp/eljdnd.o \
    wxHaskell/wxc/src/cpp/eljdrawing.o \
    wxHaskell/wxc/src/cpp/eljevent.o \
    wxHaskell/wxc/src/cpp/eljfiledialog.o \
    wxHaskell/wxc/src/cpp/eljfilehist.o \
    wxHaskell/wxc/src/cpp/eljfindrepldlg.o \
    wxHaskell/wxc/src/cpp/eljfont.o \
    wxHaskell/wxc/src/cpp/eljfontdata.o \
    wxHaskell/wxc/src/cpp/eljfontdlg.o \
    wxHaskell/wxc/src/cpp/eljframe.o \
    wxHaskell/wxc/src/cpp/eljgauge.o \
    wxHaskell/wxc/src/cpp/eljgrid.o \
    wxHaskell/wxc/src/cpp/eljhelpcontroller.o \
    wxHaskell/wxc/src/cpp/eljicnbndl.o \
    wxHaskell/wxc/src/cpp/eljicon.o \
    wxHaskell/wxc/src/cpp/eljimage.o \
    wxHaskell/wxc/src/cpp/eljimagehandler.o \
    wxHaskell/wxc/src/cpp/eljimagelist.o \
    wxHaskell/wxc/src/cpp/eljipc.o \
    wxHaskell/wxc/src/cpp/eljjoystick.o \
    wxHaskell/wxc/src/cpp/eljlayoutconstraints.o \
    wxHaskell/wxc/src/cpp/eljlistbox.o \
    wxHaskell/wxc/src/cpp/eljlistctrl.o \
    wxHaskell/wxc/src/cpp/eljlocale.o \
    wxHaskell/wxc/src/cpp/eljlog.o \
    wxHaskell/wxc/src/cpp/eljmask.o \
    wxHaskell/wxc/src/cpp/eljmdi.o \
    wxHaskell/wxc/src/cpp/eljmenu.o \
    wxHaskell/wxc/src/cpp/eljmenubar.o \
    wxHaskell/wxc/src/cpp/eljmessagedialog.o \
    wxHaskell/wxc/src/cpp/eljmime.o \
    wxHaskell/wxc/src/cpp/eljminiframe.o \
    wxHaskell/wxc/src/cpp/eljnotebook.o \
    wxHaskell/wxc/src/cpp/eljpalette.o \
    wxHaskell/wxc/src/cpp/eljpanel.o \
    wxHaskell/wxc/src/cpp/eljpen.o \
    wxHaskell/wxc/src/cpp/eljprintdlg.o \
    wxHaskell/wxc/src/cpp/eljprinting.o \
    wxHaskell/wxc/src/cpp/eljprocess.o \
    wxHaskell/wxc/src/cpp/eljradiobox.o \
    wxHaskell/wxc/src/cpp/eljradiobutton.o \
    wxHaskell/wxc/src/cpp/eljrc.o \
    wxHaskell/wxc/src/cpp/eljregion.o \
    wxHaskell/wxc/src/cpp/eljregioniter.o \
    wxHaskell/wxc/src/cpp/eljsash.o \
    wxHaskell/wxc/src/cpp/eljscrollbar.o \
    wxHaskell/wxc/src/cpp/eljscrolledwindow.o \
    wxHaskell/wxc/src/cpp/eljsingleinst.o \
    wxHaskell/wxc/src/cpp/eljsizer.o \
    wxHaskell/wxc/src/cpp/eljslider.o \
    wxHaskell/wxc/src/cpp/eljspinctrl.o \
    wxHaskell/wxc/src/cpp/eljsplash.o \
    wxHaskell/wxc/src/cpp/eljsplitterwindow.o \
    wxHaskell/wxc/src/cpp/eljstaticbox.o \
    wxHaskell/wxc/src/cpp/eljstaticline.o \
    wxHaskell/wxc/src/cpp/eljstatictext.o \
    wxHaskell/wxc/src/cpp/eljstatusbar.o \
    wxHaskell/wxc/src/cpp/eljsystemsettings.o \
    wxHaskell/wxc/src/cpp/eljtextctrl.o \
    wxHaskell/wxc/src/cpp/eljtglbtn.o \
    wxHaskell/wxc/src/cpp/eljthread.o \
    wxHaskell/wxc/src/cpp/eljtimer.o \
    wxHaskell/wxc/src/cpp/eljtipwnd.o \
    wxHaskell/wxc/src/cpp/eljtoolbar.o \
    wxHaskell/wxc/src/cpp/eljvalidator.o \
    wxHaskell/wxc/src/cpp/eljwindow.o \
    wxHaskell/wxc/src/cpp/eljwizard.o \
    wxHaskell/wxc/src/cpp/ewxw_main.o \
    wxHaskell/wxc/src/cpp/ewxw_main.o \
    wxHaskell/wxc/src/cpp/extra.o \
    wxHaskell/wxc/src/cpp/glcanvas.o \
    wxHaskell/wxc/src/cpp/graphicscontext.o \
    wxHaskell/wxc/src/cpp/hyperlinkctrl.o \
    wxHaskell/wxc/src/cpp/image.o \
    wxHaskell/wxc/src/cpp/managed.o \
    wxHaskell/wxc/src/cpp/mediactrl.o \
    wxHaskell/wxc/src/cpp/pickerctrl.o \
    wxHaskell/wxc/src/cpp/previewframe.o \
    wxHaskell/wxc/src/cpp/printout.o \
    wxHaskell/wxc/src/cpp/sckaddr.o \
    wxHaskell/wxc/src/cpp/socket.o \
    wxHaskell/wxc/src/cpp/sound.o \
    wxHaskell/wxc/src/cpp/stc.o \
    wxHaskell/wxc/src/cpp/std.o \
    wxHaskell/wxc/src/cpp/stream.o \
    wxHaskell/wxc/src/cpp/taskbaricon.o \
    wxHaskell/wxc/src/cpp/textstream.o \
    wxHaskell/wxc/src/cpp/treectrl.o \
    wxHaskell/wxc/src/cpp/wrapper.o \

all: $(BIN_FILE)

sheep: $(OBJ_FILES) $(WXC_OBJ_FILES)
	$(LD) $@ $(LD_FLAGS) $^

%.o: %.c
	$(CC) $(CC_FLAGS) $(CC_RELEASE_FLAGS) -c -o $@ $<

%.o: %.cpp
	$(CXX) $(CXX_FLAGS) $(CXX_RELEASE_FLAGS) -c -o $@ $<

run: sheep
	./sheep

valgrind: sheep
	valgrind ./sheep

clean:
	- rm -f $(OBJ_FILES) $(BIN_FILE)

clean-all:
	- rm -f $(OBJ_FILES) $(WXC_OBJ_FILES) $(BIN_FILE)

